#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Neote -- A note taking application
#
# Copyright (C) 2009 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/neote/
#
# This file is part of Neote.
#
# Neote is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Neote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Neote is a note taking application
"""

APP_NAME    = 'Neote'
APP_WM_INFO = ("Neote", "neote")
APP_VERSION = '0.2.0'

from sys import argv
from optparse import OptionParser
from gui import *

class Neote(object):
    def __init__(self, verbose=False):
        self.verbose = verbose
        self.win = elementary.Window(APP_NAME, elementary.ELM_WIN_BASIC)
        self.win.title_set(APP_NAME)
        self.win.destroy = (self.quit, None)

        bg = elementary.Background(self.win)
        self.win.resize_object_add(bg)
        bg.size_hint_weight_set(1, 1)
        bg.show()

        box = elementary.Box(self.win)
        self.win.resize_object_add(box)
        box.size_hint_weight_set(1, 1)
        box.show()

        toolbar = elementary.Toolbar(self.win)
        toolbar.size_hint_align_set(-1.0, 0)

        self.toolbar_item_add_note = toolbar.item_add(
            NeoteIcon(self.win, 'note-add.png'), "Add Note", self.show_add_note_page)
        self.toolbar_item_notes = toolbar.item_add(
            NeoteIcon(self.win, 'notes.png'), "Notes", self.show_notes_page)
        self.toolbar_item_search = toolbar.item_add(
            NeoteIcon(self.win, 'note-search.png'), "Search", self.show_search_page)
        self.toolbar_item_preferences = toolbar.item_add(
            NeoteIcon(self.win, 'preferences.png'), "Preferences", self.show_preferences)

        box.pack_end(toolbar)
        toolbar.show()

        self.pager = elementary.Pager(self.win)
        self.pager.size_hint_weight_set(1.0, 1.0)
        self.pager.size_hint_align_set(-1.0, -1.0)
        box.pack_end(self.pager)
        self.pager.show()

        self.add_note_page = AddNotePage(self)
        self.notes_page = NotesPage(self)
        self.search_page = SearchPage(self)
        self.preferences_page = PreferencesPage(self)
        self.toolbar_item_add_note.select()

        self.win.resize(480, 640)
        self.win.show()

    def quit(self, obj, event, data):
        if self.verbose:
            print "Bye bye, thx to use %s" % APP_NAME

        self.notes_page.save_note()
        db.close()
        elementary.exit()

    def show_add_note_page(self, *args):
        self.pager.content_promote(self.add_note_page.box)

    def show_notes_page(self, *args):
        self.toolbar_item_notes.select()
        self.pager.content_promote(self.notes_page.box)

    def show_search_page(self, *args):
        self.pager.content_promote(self.search_page.box)

    def show_preferences(self, *args):
        self.pager.content_promote(self.preferences_page.box)


def main():
    parser = OptionParser('usage: %prog [options]')
    parser.add_option('-i', '--import', dest='import_notes',
        help="Import file containing notes")
    parser.add_option('-v', '--verbose', action='store_true', default=False, help="Display more messages")
    options, arguments = parser.parse_args(argv[1:])
    if len(arguments) != 0:
        parser.error("Incorrect number of arguments")
    if options.import_notes:
        import importer
        importer.import_notes(options.import_notes, verbose=options.verbose)
        return 0
    else:
        elementary.init()
        Neote(options.verbose)
        elementary.run()
        elementary.shutdown()
        return 0

if __name__ == "__main__":
    exit(main())
