     _   _            _
    | \ | | ___  ___ | |_ ___
    |  \| |/ _ \/ _ \| __/ _ \
    | |\  |  __/ (_) | ||  __/
    |_| \_|\___|\___/ \__\___|

Description
===========
Neote is a note taking application.

Its interface aims to be easy and finger friendly.
It uses SQLite for storing data.

Notes can be organized using custom categories that you can add, rename
and delete at any time.

https://gitlab.com/valos/neote


Authors
=======
Valéry Febvre <vfebvre@easter-eggs.com>


Requirements
============
- python
- python-elementary
- python-sqlite3


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/neote/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Tango Desktop Project for icons (http://tango.freedesktop.org/)


Version History
===============

2009-07-07 - Version 0.1.1
--------------------------
- This release fixes a folder creation problem in 0.1.0.

2009-07-07 - Version 0.1.0
--------------------------
- First release.
