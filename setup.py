# -*- coding: utf-8 -*-

# Neote -- A note taking application
#
# Copyright (C) 2009 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/neote/
#
# This file is part of Neote.
#
# Neote is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Neote is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def get_images_list():
    l = []
    for root, dirs, files in os.walk('data/images/', topdown = False):
        if root != 'data/images/': continue
        for name in files:
            l.append(os.path.join(root, name))
    return l

def main():
    setup(name         = 'neote',
          version      = '0.2.0',
          description  = 'a note taking application',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/neote/',
          classifiers  = [
            'Development Status :: 2 - Beta',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['neote'],
          scripts      = ['neote/neote'],
          data_files   = [
            ('share/applications', ['data/neote.desktop']),
            ('share/pixmaps', ['data/neote.png']),
            ('share/neote', ['README']),
            ('share/neote/images', get_images_list()),
            ],
          )

if __name__ == '__main__':
    main()
